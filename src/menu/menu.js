import React, {Component} from 'react';
import Leftmenu from '../layout/leftmenu.js'
import Headers from '../layout/header.js'
import { Layout } from 'antd';
import Procontent from '../produce/procontent.js';
import './menu.css';

const { Header, Sider, Content } = Layout;

class Menu extends Component{
	constructor(props) {
	    super(props)
		this.state = {}
	}
	render(){
		return(
			<div>
				<Layout>
					<Sider>
						<Leftmenu />
					</Sider>
					<Layout>
						<Header>
							<Headers />
						</Header>
						<Content className='content'>
							<Procontent />
						</Content>
					</Layout>
				</Layout>
				
			</div>
		)
	}
}
export default Menu;