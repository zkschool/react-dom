import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from "history";
import Login from './login/login.js';
import Menu from './menu/menu.js';

export default class App extends React.Component{
	render(){
		return(
			<Router history={createBrowserHistory()}>
				<Switch>
					<Route exact path="/" component={Login} />,
					<Menu>
						<Route path="/Menu" component={Menu} />
					</Menu>
				</Switch>
			</Router>
		)
	}
}