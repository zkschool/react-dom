const router = [
	{
		title: '菜单一',
		key: "/firstpage/firstpage"
	},{
		title: '菜单二',
		key: "/secondpage",
		child: [
			{
				title: '子菜单一',
				key: "/secondpage/second"
			},{
				title: '子菜单二',
				key: "/less/less"
			}
		]
	}
]
export default router;