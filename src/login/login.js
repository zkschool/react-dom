import React, {Component} from 'react';
import { Input, message } from 'antd';
import './login.css'
class Login extends Component{
	constructor(props) {
	    super(props)
		this.state = {
			user: 'admin',
			psw: '123456'
		}
	}
	handChangeUser = (e) => {
		this.setState({
			user: e.target.value
		})
	}
	handChangePsw = (e) => {
		this.setState({
			psw: e.target.value
		})
	}
	handClickLogin = () => {
		if(this.state.user === 'admin' && this.state.psw === '123456'){
			this.props.history.replace('/firstpage/firstpage')
		}else{
			message.info('请输入正确的账号和密码');
		}
	}
	render(){
		return(
			<div className='login'>
				<div className='login-pos'>
					<div className='login_title'>密码登录</div>
					<div className='login-input'>
						<Input value={this.state.user} onChange={this.handChangeUser} placeholder="请输入账号" />
						<Input className='login-input_child' type="password" value={this.state.psw} onChange={this.handChangePsw} placeholder="请输入密码" />
					</div>
					<div className='login_but' onClick={this.handClickLogin}>登录</div>
				</div>
			</div>
		)
	}
}
export default Login;