import React, {Component} from 'react';
import { Menu } from 'antd';
import './leftmenu.css';
import {Link} from 'react-router-dom';
import Router from '../router/router.js';
const { SubMenu } = Menu;

class Leftmenu extends Component{
	constructor(props) {
	    super(props)
		this.state = {}
	}
	renderMenu = ({ title, key }) => {
		return (
	      <Menu.Item key={key}>
	        <Link to={key}>{title}</Link>
	      </Menu.Item>
	    );
	}
	renderSubMenu = ({ title, key, child }) => {
	    return (
	      <SubMenu key={key} title={title}>
	        {child &&
	          child.map((item) => {
	            return item.child && item.child.length > 0
	              ? this.renderSubMenu(item)
	              : this.renderMenu(item);
	          })}
	      </SubMenu>
	    )
	}
	render(){
		return(
			<div className='leftmenu'>
				<div className='leftmenu_top'></div>
				<Menu defaultSelectedKeys={[Router[0].key]} mode="inline" theme="dark">
					{Router &&
					    Router.map((firstItem) => {
					        return firstItem.child && firstItem.child.length > 0
					            ? this.renderSubMenu(firstItem)
					            : this.renderMenu(firstItem);
					})}
				</Menu>
			</div>
		)
	}
}
export default Leftmenu;