import React, {Component} from 'react';
import { withRouter } from "react-router-dom";
import './leftmenu.css'

class Header extends Component{
	handBackLogin = () => {
		this.props.history.replace('/')
	}
	render(){
		return(
			<div className='header'>
				<div className='outlogin' onClick={this.handBackLogin}>退出登录</div>
			</div>
		)
	}
}
export default withRouter(Header);