import React, {Component} from 'react';
import axios from 'axios';
import { Table } from 'antd';
import {store, firstpage} from '../store/store.js';
import {handlistdata} from '../store/actionCreatores.js'

const columns = [
		{
			title: '班级',
			dataIndex: 'classNickName'
		},{
			title: '喝水人数',
			dataIndex: 'drinkWaterPeopleNum'
		},{
			title: '占比',
			dataIndex: 'drinkWaterPercentage'
		},{
			title: '时间',
			dataIndex: 'time'
		},
]
export default class Firstpage extends Component{
	constructor(props) {
	    super(props)
		this.state = {
			data: []
		}
		console.log('store的数据',store.getState())
	}
	componentDidMount(){
		axios.get('http://edu-iot.nullehome.com:9001/api/water/class/table/form/statistics/1206810637223247873').then(res => {
			if(res.data.code === 0){
				const action = handlistdata(res.data.data);
				firstpage.dispatch(action)
				this.setState({
					data: firstpage.getState().list
				})
			}
		})
	}
	render(){
		return(
			<Table columns={columns} dataSource={this.state.data} rowKey={(r) => r.classNickName} />
		)
	}
}