import {change_data, click_data, delete_data} from './actionType.js'

const defaultState = {  //默认数据
	inputValue : 'Write Something',
	list:[]
}
export default (state = defaultState,action)=>{  //就是一个方法函数
	if(action.type === change_data){
		let newdata = JSON.parse(JSON.stringify(state));
		newdata.inputValue = action.value;
		return newdata;
	}
	if(action.type === click_data){
		let newdata = JSON.parse(JSON.stringify(state));
		newdata.list.push(newdata.inputValue);
		return newdata;
	}
	if(action.type === delete_data){
		let newdata = JSON.parse(JSON.stringify(state));
		newdata.list.splice(action.value, 1);
		return newdata;
	}
    return state
}