import { createStore } from 'redux';  // 引入createStore方法
import reducer from './reducer.js';
import firstreducer from './firstreducer.js'

export const store = createStore(reducer);          // 创建数据存储仓库，唯一，不能创建多个
export const firstpage = createStore(firstreducer);