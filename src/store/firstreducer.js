import {first_list} from './actionType.js'

const defaultState = {
	list: [],
}

export default (state = defaultState, action) => {
	if(action.type === first_list){
		let newdata = JSON.parse(JSON.stringify(state));
		newdata.list = action.list;
		return newdata;
	}
}