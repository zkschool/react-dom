import {change_data, click_data, delete_data, first_list} from './actionType.js'

export const handchangedata = (value) => ({
	type: change_data,
	value
})
export const handclickdata = () => ({
	type: click_data
})
export const handdeletedata = (value) => ({
	type: delete_data,
	value
})
export const handlistdata = (list) => ({
	type: first_list,
	list
})