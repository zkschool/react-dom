import React from 'react';
import { Input , Button , List } from 'antd';

const Secondui = (props) => {
	return(
		<div style={{margin:'10px'}}>
		    <div>
		        <Input value={props.inputValue} onChange={props.handChangeData} style={{width: 200}} />
		        <Button type="primary" onClick={props.handClickPush}>增加</Button>
		    </div>
			<div>store自动推送数据{props.inputValue}</div>
		    <div>
		        <List bordered dataSource={props.list} renderItem={(item, index)=>(<List.Item onClick={() => {props.handDeleteList(index)}}>{item}</List.Item>)} />
		    </div>
		</div>
	)
}
export default Secondui;