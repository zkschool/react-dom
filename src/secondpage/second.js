import React, {Component} from 'react';
import {store} from '../store/store.js';
import {handchangedata, handclickdata, handdeletedata} from '../store/actionCreatores.js';
import Secondui from './secondUi.js';

export default class Second extends Component{
	constructor(props) {
	    super(props)
		this.state = {
			list: store.getState().list,
			inputValue: store.getState().inputValue
		}
		this.handDeleteList = this.handDeleteList.bind(this)
		store.subscribe(this.storeChangeData)
	}
	handChangeData = (e) => {
		const action = handchangedata(e.target.value)
		store.dispatch(action)
	}
	storeChangeData = () =>{
		this.setState({
			inputValue: store.getState().inputValue
		})
	}
	handClickPush = () => {
		const action = handclickdata()
		store.dispatch(action)
		this.setState({
			list: store.getState().list,
			inputValue: ''
		})
	}
	handDeleteList(index){
		const action = handdeletedata(index)
		store.dispatch(action)
		this.setState({
			list: store.getState().list
		})
	}
	render(){
		return(
			<Secondui list={this.state.list} 
			inputValue={this.state.inputValue} 
			handChangeData={this.handChangeData} 
			handClickPush={this.handClickPush} 
			handDeleteList={this.handDeleteList} />
		)
	}
}