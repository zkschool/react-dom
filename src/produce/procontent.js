import React, {Component} from 'react';
import { Route, Switch } from "react-router-dom";
import Firstpage from '../firstpage/firstpage.js';
import Second from '../secondpage/second.js';
import Less from '../less/less.js';

class Procontent extends Component{
	render(){
		return(
			<>
				<Switch>
					<Route exact path="/firstpage/firstpage" component={Firstpage}></Route>,
					<Route exact path="/secondpage/second" component={Second}></Route>,
					<Route exact path="/less/less" component={Less}></Route>
				</Switch>
			</>
		)
	}
}
export default Procontent;